"use strict";

const myBox = document.querySelector(".box-animate");
myBox.addEventListener("click", () => {
  myBox.classList.add("float-animation-reverse");
  myBox.classList.toggle("float-animation");
});


const myBall = document.querySelector(".ball");
myBall.addEventListener("click", () => myBall.classList.toggle("bounce-ball"));